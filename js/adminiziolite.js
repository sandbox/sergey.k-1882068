(function($){
    $(document).ready(function(){
        $('#col1-switcher').click(function(){
            $(document.body).removeClass('sidebar-left').removeClass('one-sidebar').addClass('no-sidebar');
            $.cookie('sidebar', 0, {path: '/', expires: 7});
        });

        $('#col2-switcher').click(function(){
            $(document.body).addClass('sidebar-left').addClass('one-sidebar').removeClass('no-sidebar');
            $.cookie('sidebar', 1, {path: '/', expires: 7});
        });
    });
})(jQuery);
