<?php
/**
 * @file
 * Displays a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $css: An array of CSS files for the current page.
 * - $directory: The directory the theme is located in, e.g. themes/garland or
 *   themes/garland/minelli.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Page metadata:
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or
 *   'rtl'.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   element.
 * - $head: Markup for the HEAD element (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $body_classes: A set of CSS classes for the BODY tag. This contains flags
 *   indicating the current layout (multiple columns, single column), the
 *   current path, whether the user is logged in, and so on.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled in
 *   theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $mission: The text of the site mission, empty when display has been
 *   disabled in theme settings.
 *
 * Navigation:
 * - $search_box: HTML to display the search box, empty if search has been
 *   disabled.
 * - $primary_links (array): An array containing primary navigation links for
 *   the site, if they have been configured.
 * - $secondary_links (array): An array containing secondary navigation links
 *   for the site, if they have been configured.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $left: The HTML for the left sidebar.
 * - $breadcrumb: The breadcrumb trail for the current page.
 * - $title: The page title, for use in the actual HTML content.
 * - $help: Dynamic help text, mostly for admin pages.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - $content: The main content of the current Drupal page.
 * - $right: The HTML for the right sidebar.
 * - $node: The node object, if there is an automatically-loaded node associated
 *   with the page, and the node ID is the second argument in the page's path
 *   (e.g. node/12345 and node/12345/revisions, but not comment/reply/12345).
 *
 * Footer/closing data:
 * - $feed_icons: A string of all feed icons for the current page.
 * - $footer_message: The footer message as defined in the admin settings.
 * - $footer : The footer region.
 * - $closure: Final closing markup from any modules that have altered the page.
 *   This variable should always be output last, after all other dynamic
 *   content.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>
    <?php print $scripts; ?>
    <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?></script>
    <!--[if lt IE 9]><script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body class="<?php print $body_classes; ?>">
    <section id="root"><!-- root -->
        <section id="doc"><!-- doc -->
            <header>
                <div id="tray" class="clear-block"><!-- tray -->
                    <p class="f-left clear-block">
                        <span class="f-left" id="switcher">
                            <img id="col1-switcher" src="<?php echo $front_page.$directory.'/'; ?>img/switcher-1col.gif" alt="1 Column" />
                            <img id="col2-switcher" src="<?php echo $front_page.$directory.'/'; ?>img/switcher-2col.gif" alt="2 Columns" />
                        </span>
                        Project: <strong><?php echo $site_name; ?></strong>
                    </p>
                    <p class="f-right"><?php if($logged_in): ?>User: <strong><a href="<?php echo url('user'); ?>"><?php echo $GLOBALS['user']->name; ?></a></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><a href="<?php echo url('logout'); ?>" id="logout">Log out</a></strong><?php else: ?><strong><a href="<?php echo url('user'); ?>" id="login">Log in</a></strong><?php endif; ?></p>
                </div><!-- /tray -->

                <div id="menu" class="clear-block"><!-- menu -->
                    <ul class="clear-block f-right"><li><a href="<?php echo url('<front>'); ?>"><span><strong>Visit Site &rarr; </strong></span></a></li></ul>
                    <?php if(isset($adminiziolite_links) && sizeof($adminiziolite_links)) echo theme('adminiziolite_links', $adminiziolite_links, array('class' => 'clear-block f-left', 'id' => 'adminiziolite-menu')); ?>
                </div><!-- /menu -->
            </header>

            <section id="bd" class="clear-block"><!-- /bd -->
                <section id="left"><!-- left -->
                    <?php if ($logo): ?>
                        <p id="logo"><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><img src="<?php print($logo) ?>" alt="<?php print t('Home') ?>" border="0" /></a></p>
                    <?php endif; ?>

                    <?php if(isset($search_box)) echo $search_box; ?>

                    <?php if($logged_in): ?>
                        <p id="btn-create" class="clear-block"><a href="<?php echo url('node/add'); ?>"><span><?php echo t('Create content'); ?></span></a></p>
                    <?php endif; ?>

                    <?php echo $left; ?>
                </section><!-- /left -->
                <section id="main"><!-- main -->
                    <?php if ($title): print '<h1 id="page-title">'. $title .'</h1>'; endif; ?>

                    <?php echo $breadcrumb; ?>

                    <?php if(isset($tabs)): ?>
                    <div id="tabs">
                        <?php echo $tabs; ?>
                    </div>
                    <?php endif; ?>

                    <?php if($show_messages && $messages) echo $messages; ?>

                    <?php if(isset($help)) echo $help; ?>

                    <?php echo $content; ?>
                </section><!-- /main -->
            </section><!-- /bd -->

            <section id="footer-space"></section>
        </section><!-- /doc -->

        <footer>
            <div class="content clear-block">
                <?php if($footer_message): ?>
                    <p class="f-left"><?php echo $footer_message; ?></p>
                <?php endif; ?>
                <p class="f-right">Drupal theme by <a href="http://coollider.com/">coollider.com</a>, template by <a href="http://www.adminizio.com/">Adminizio</a></p>
            </div>
        </footer>
    </section><!-- /root -->

    <?php if(isset($closure)): ?><!-- closure --><?php echo $closure; ?><!-- /closure --><?php endif; ?>
</body>
</html>
