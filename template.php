<?php
/**
 * Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
 */
function adminizio_lite_menu_local_tasks() {
    $output = '';

    // CTools requires a different set of local task functions.
    if(function_exists('ctools_menu_primary_local_tasks') & function_exists('ctools_menu_secondary_local_tasks')){
        $primary = ctools_menu_primary_local_tasks();
        $secondary = ctools_menu_secondary_local_tasks();
    }
    else {
        $primary = menu_primary_local_tasks();
        $secondary = menu_secondary_local_tasks();
    }

    if($primary){
        $output .= '<ul id="primary-tabs" class="tabs clear-block">' . $primary . '</ul>';
    }
    if($secondary){
        $output .= '<ul id="secondary-tabs" class="tabs clear-block">' . $secondary . '</ul>';
    }

    return $output;
}

/**
 * Return a themed sort icon.
 *
 * @param $style
 *   Set to either asc or desc. This sets which icon to show.
 * @return mixed
 *   A themed sort icon.
 */
function adminizio_lite_tablesort_indicator($style){
    global $theme_path;

    if($style == 'asc'){
        return theme('image', $theme_path.'/img/arrow/asc.png', t('sort icon'), t('sort ascending'));
    }else{
        return theme('image', $theme_path.'/img/arrow/desc.png', t('sort icon'), t('sort descending'));
    }
}

/**
 * Format a form.
 *
 * @param $element
 *   An associative array containing the properties of the element.
 *   Properties used: action, method, attributes, children
 * @return string
 *   A themed HTML string representing the form.
 *
 * @ingroup themeable
 */
function adminizio_lite_form($element) {
    // Anonymous div to satisfy XHTML compliance.
    $action = $element['#action'] ? 'action="'. check_url($element['#action']) .'" ' : '';
    return '<form '. $action .' accept-charset="UTF-8" method="'. $element['#method'] .'" id="'. $element['#id'] .'"'. drupal_attributes($element['#attributes']) .">\n<div class=\"form-content\">". $element['#children'] ."\n</div></form>\n";
}

/**
 * Implements adminizio_lite_menu_item_link()
 */
function adminizio_lite_menu_item_link($link){
    if(empty($link['localized_options'])){
        $link['localized_options'] = array();
    }

    // If an item is a LOCAL TASK, render it as a tab
    if($link['type'] & MENU_IS_LOCAL_TASK){
        $link['title'] = '<span class="tab">' . check_plain($link['title']) . '</span>';
        $link['localized_options']['html'] = TRUE;
    }

    return l($link['title'], $link['href'], $link['localized_options']);
}

/**
 * @param $breadcrumb
 * @return string
 */
function adminizio_lite_breadcrumb($breadcrumb) {
    $html = null;
    if(1 < ($bl = sizeof($breadcrumb))){
        $separator = '&raquo;';
        $i = 0; $bl--;
        while($i < $bl){
            $breadcrumb[$i].= ' '.$separator.' ';
            $i++;
        }
        $html = '<div id="breadcrumb">'.theme('item_list', $breadcrumb).'</div>';
    }
    return $html;
}

/**
 * @return null|string
 */
function adminizio_lite_help(){
    if($help = menu_get_active_help()){
        return '<div id="help">'. $help .'</div>';
    }

    return null;
}

/**
 * @param null $display
 * @return string
 */
function adminizio_lite_status_messages($display = NULL) {
    $output = '';
    foreach(drupal_get_messages($display) as $type => $messages){
        $output .= "<div class=\"messages $type\">\n";
        if(count($messages) > 1){
            $output .= " <ul>\n";
            foreach($messages as $message){
                $output .= '  <li>'. $message ."</li>\n";
            }
            $output .= " </ul>\n";
        }else{
            $output .= $messages[0];
        }
        $output .= "</div>\n";
    }
    return empty($output)?null:('<div id="messages">'.$output.'</div>');
}

/**
 * Implements hook_theme()
 */
function adminizio_lite_theme($existing, $type, $theme, $path){
    // add adminizio_lite menu if not exist
    if(!menu_load('menu-adminiziolite')){
        $form_state = array(
            'values' => array(
                'menu_name' => 'adminiziolite',
                'title' => 'Adminiziolite menu',
                'description' => 'Adminiziolite menu used at the theme layer to show the major sections of an admin parth of site.'
            )
        );

        menu_edit_menu_submit(array('#insert' => true), $form_state);
    }

    // register adminiziolite_links theme function
    return array(
        'adminiziolite_links' => array(
            'arguments' => array('links' => array(), 'attributes' => array('class' => 'clear-block'))
        )
    );
}

/**
 * @param $variables
 * @param $hook
 */
function adminizio_lite_preprocess_search_theme_form(&$variables, $hook){
    $variables['form']['search_theme_form']['#title'] = null;
    $variables['form']['search_theme_form']['#value'] = t('Search').'...';
    $variables['form']['search_theme_form']['#size'] = 12;
    $variables['form']['search_theme_form']['#attributes']['onclick'] = 'if(this.value == "'.t('Search').'..."){this.value = "";}';
    $variables['form']['search_theme_form']['#attributes']['onblur'] = 'if($.trim(this.value) == ""){this.value = "'.t('Search').'...";}';
    unset($variables['form']['search_theme_form']['#printed']);
    $variables['search']['search_theme_form'] = drupal_render($variables['form']['search_theme_form']);
}

/**
 * @param array $links
 * @param array $attributes
 * @return string
 */
function adminizio_lite_adminiziolite_links(array $links, array $attributes = array('class' => 'clear-block')){
    global $language;
    $output = '';

    if (count($links) > 0) {
        $output = '<ul'. drupal_attributes($attributes) .'>';

        $num_links = count($links);
        $i = 1;

        foreach ($links as $key => $link) {
            $class = $key;

            // Add first, last and active classes to the list of links to help out themers.
            if ($i == 1) {
                $class .= ' first';
            }
            if ($i == $num_links) {
                $class .= ' last';
            }
            if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
                && (empty($link['language']) || $link['language']->language == $language->language)) {
                $class .= ' active';
            }
            $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';

            if (isset($link['href'])) {
                // Pass in $link as $options, they share the same keys.
                $link['html'] = true; $output .=  l('<span>'.$link['title'].'</span>', $link['href'], $link);
            }
            else if (!empty($link['title'])) {
                // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
                if (empty($link['html'])) {
                    $link['title'] = check_plain($link['title']);
                }
                $span_attributes = '';
                if (isset($link['attributes'])) {
                    $span_attributes = drupal_attributes($link['attributes']);
                }
                $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
            }

            $i++;
            $output .= "</li>\n";
        }

        $output .= '</ul>';
    }

    return $output;
}

/**
 * @param $variables
 */
function adminizio_lite_preprocess_page(&$variables){
    $variables['adminiziolite_links'] = menu_navigation_links('menu-adminiziolite');

    if(isset($_COOKIE['sidebar']) && !(int)$_COOKIE['sidebar']){
        $variables['body_classes'] = str_replace(' one-sidebar ', ' no-sidebar ', $variables['body_classes']);
        $variables['body_classes'] = str_replace(' sidebar-left ', ' ', $variables['body_classes']);
    }
}







